#!/usr/bin/env python3
import sys
from setuptools import setup
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('--without-tools', action='store_true')
args, unknown = parser.parse_known_args()

sys.argv = [sys.argv[0]] + unknown

long_description = "TODO"
# long_description = open(
#     os.path.join(
#         os.path.dirname(__file__),
#         'README.rst'
#     )
# ).read()

install_requires = [
    'trio~=0.13.0'
]

packages = [
    'ohm2013_ledstrip_control',
    'ohm2013_ledstrip_control.effects'
]

entry_points = None

if not args.without_tools:

    packages.append('ohm2013_ledstrip_control.tools')
    install_requires.append('pygame>=1.9.3')
    entry_points = {
        'console_scripts': [
            'ohm2013-simstrip = ohm2013_ledstrip_control.tools.simstrip:run',
            'ohm2013-scan = ohm2013_ledstrip_control.tools.scan:run',
            'ohm2013-run-effect = ohm2013_ledstrip_control.tools.run_effect:run' # noqa
        ]
    }

    if sys.platform == 'linux':
        install_requires.append('python3-xlib>=0.15')

setup(name='ohm2013-ledstrip-control',
      author='Lorian Coltof',
      author_email='competitie@svia.nl',
      url='https://github.com/viaict/bitlair-ohm2013-ledstrip-control',
      description='Library for controlling Bitlair ledstrip sleeves',
      long_description=long_description,
      packages=packages,
      version='0.1.0',
      install_requires=install_requires,
      python_requires='>=3.5',
      entry_points=entry_points)
