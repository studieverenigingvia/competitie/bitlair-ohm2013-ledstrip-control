import trio
import trio.socket as socket
import traceback
import signal

from abc import ABC, abstractmethod
from enum import Enum


def _clip_color(color):
    return tuple(max(min(int(c), 0xff), 0) for c in color)


class LightConnection:
    def __init__(self, sock, length, brightness=1):
        self.sock = sock
        self.length = length
        self.brightness = brightness
        self.peername = sock.getpeername()
        self._closed = False

    async def send(self, data):
        if len(data) > self.length:
            raise ValueError(
                "Length of 'data' exceeds {}".format(self.length))

        bin_data = bytearray(b"Art-Net\x00\x00\x50\x00\x0e\x00\x00\x00\x00\x02\x00")  # noqa
        bin_data.extend(b'\x00\x00\x00' * (self.length - len(data)))
        for color in data[::-1]:
            r, g, b = color
            r = int(r * self.brightness)
            g = int(g * self.brightness)
            b = int(b * self.brightness)
            bin_data.extend([r, g, b])

        await self.sock.send(bin_data)

    async def clear(self):
        await self.send([(0, 0, 0)] * self.length)

    def __str__(self):
        return "LightConnection {}{}".format(
            self.peername, " closed" if self._closed else "")


class Strip:
    def __init__(self, shape):
        if isinstance(shape, int):
            shape = (shape, )

        self.shape = shape

        self.data = [(0, 0, 0) for i in range(shape[-1])]
        for length in shape[:-1]:
            self.data = [list(self.data) for i in range(length)]

    def get_data(self):
        data = self.data
        while isinstance(data[0], list):
            newdata = []
            for d in data:
                newdata.extend(d)
            data = newdata

        return data

    def _fill(self, data, color):
        if isinstance(data[0], list):
            for d in data:
                self._fill(d, color)
        else:
            color = _clip_color(color)
            for i in range(len(data)):
                data[i] = color

    def fill(self, color):
        self._fill(self.data, color)

    def clear(self):
        self.fill((0, 0, 0))

    def __check_key(self, key):
        if isinstance(key, int) or isinstance(key, slice):
            key = (key, )

        if isinstance(key, tuple):
            if len(key) > len(self.shape):
                raise IndexError('too many indices for {}D strip'.format(
                                 len(self.shape)))
        else:
            raise IndexError(
                'only integers and slices (`:`) are valid indices')

        return key

    def __getitem__(self, key):
        key = self.__check_key(key)
        d = self.data
        for index in key:
            d = d[index]
        return d

    def __setitem__(self, key, value):
        key = self.__check_key(key)
        if isinstance(value, tuple) and len(value) == 3:
            d = self.data
            for index in key[:-1]:
                d = d[index]

            if not isinstance(d[key[-1]], tuple):
                raise IndexError("index should point to a single color value")

            d[key[-1]] = _clip_color(value)
        else:
            raise ValueError("value should be a tuple of three integers")

    def __delitem__(self, key):
        self[key] = (0, 0, 0)

    def __str__(self):
        return "<{}D {}>".format(len(self.shape), self.__class__.__name__)

    def __repr__(self):
        return str(self)


class Effect(ABC):
    def __init__(self, step_time=0.02, strip_shape=(21, 7)):
        self.step_time = step_time
        self.strip_shape = strip_shape
        self.quit = False

    def __iter__(self):
        strip = Strip(self.strip_shape)
        self.setup(strip)
        count = 0
        while True:
            self.other_effect_request = None
            res = self.step(strip, count)
            if self.other_effect_request:
                yield from self.other_effect_request

            yield strip.get_data(), self.step_time

            if res is False:
                return

            count += 1

    def run_other_effect(self, effect):
        self.other_effect_request = effect

    def setup(self, strip):
        pass

    @abstractmethod
    def step(self, strip, count):
        pass

    async def run(self, lights):
        await run_frames(self, lights)


async def run_frames(frames, lights):
    lights = list(lights)
    for frame, display_time in frames:
        for light in lights:
            if light._closed:
                continue

            try:
                await light.send(frame)
            except OSError as e:
                # I am not really sure what to to here.
                # Printing a warning that sending the frame failed
                # is not that useful if you're using polls to manage
                # which lights are connected.
                # I will just ignore these errors for now
                pass
                # print("Sending frame to {} failed: {}".format(light, e))

        await trio.sleep(display_time)


class LightEventType(Enum):
    NEW_POLL_REPLY = 1,
    NO_POLL_REPLY = 2


class LightConnectionManager:
    def __init__(self, brightness=1):
        self.connections = {}
        self._closed_connections = {}
        self.brightness = brightness
        self._nursery_context_manager = None
        self._nursery = None
        self.events = {}

    @property
    def trio_nursery(self):
        return self._nursery

    @property
    def brightness(self):
        return self._brightness

    @brightness.setter
    def brightness(self, value):
        self._brightness = min(max(0, value), 1)
        for connection in self.connections.values():
            connection.brightness = self._brightness

    def run_effect(self, effect, lights):
        self._nursery.start_soon(effect.run, lights)

    def register_event_handler(self, event_type, handler):
        if event_type in self.events:
            raise ValueError(
                "Event {} already has a registered handler".format(
                    event_type))

        self.events[event_type] = handler

    def remove_event_handler(self, event_type):
        del self.events[event_type]

    def run_poll(self, addrs, bind_host="0.0.0.0", bind_port=6454,
                 reply_timeout=3, time_between_polls=10):
        self._nursery.start_soon(self._poll, addrs, bind_host, bind_port,
                                 reply_timeout, time_between_polls)

    async def _poll(self, addrs, bind_host, bind_port,
                    reply_timeout, time_between_polls):
        poll_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        poll_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        poll_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        await poll_sock.bind((bind_host, bind_port))

        addrs_full = []
        for addr in addrs:
            if isinstance(addr, str):
                host = addr
                port = 6454
            else:
                try:
                    (host, port) = addr
                    if not isinstance(port, int):
                        raise TypeError()

                except TypeError:
                    raise ValueError(
                        '{}: an address should be a string containing'
                        ' the host or a host, port tuple.'.format(addr))
            addrs_full.append((host, port))

        while True:
            for host, port in addrs_full:
                await poll_sock.sendto(b"Art-Net\x00\x00\x20\x00\x0e\x00\x80",
                                       (host, port))

            replies = set()

            current = {conn.sock.getpeername()
                       for conn in self.connections.values()
                       if not conn._closed}

            with trio.move_on_after(reply_timeout):
                while True:
                    data, (r_addr, r_port) = await poll_sock.recvfrom(5000)
                    if len(data) >= 10 and data[:8] == b'Art-Net\x00' \
                            and data[8] == 0x00 and data[9] == 0x21:
                        try:
                            mac_bin = data[201:207]
                            if mac_bin[0] == b'\x00':
                                mac = None
                            else:
                                mac = ":".join("{:02X}".format(b)
                                               for b in mac_bin)
                            long_name = data[44:108]
                            term = long_name.index(b'\x00')
                            if term:
                                long_name = long_name[:term]

                            if long_name.startswith(
                                    b"LED strip controller for OHM 2013"):
                                conn = (r_addr, r_port)

                                # Check if there is a NEW_POLL_REPLY event
                                # registered and we have a new connection
                                if LightEventType.NEW_POLL_REPLY in \
                                        self.events and conn not in current \
                                        and conn not in replies:

                                    is_reconnect = conn in \
                                        self._closed_connections
                                    try:
                                        await self.events[
                                            LightEventType.NEW_POLL_REPLY](
                                                self, r_addr, r_port,
                                                mac, is_reconnect)
                                    except Exception:
                                        traceback.print_exc()

                                # Add host, port to replies set
                                replies.add(conn)

                        except IndexError:
                            pass

            # Update connections dict

            gone_lights = current - replies
            new_connections = {}

            for key, conn in self.connections.items():
                peername = conn.sock.getpeername()
                if peername in gone_lights:
                    conn._closed = True
                    conn.sock.close()
                    self._closed_connections[key] = conn
                else:
                    new_connections[key] = conn

            self.connections = new_connections

            # Fire NO_POLL_REPLY event for gone lights if it is registered
            if LightEventType.NO_POLL_REPLY in self.events:
                for addr, port in gone_lights:
                    try:
                        await self.events[LightEventType.NO_POLL_REPLY](
                            self, addr, port)
                    except Exception:
                        traceback.print_exc()

            await trio.sleep(time_between_polls)

    async def close_all(self):
        for connection in self.connections.values():
            await self.close(connection)

        self.connections.clear()

    async def close(self, connection):
        with trio.move_on_after(1) as scope:
            scope.shield = True
            try:
                await connection.clear()
                connection.sock.close()
            except OSError:
                pass

    async def connect(self, host, port=6454, length=150):

        # Create UDP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        await sock.connect((host, port))

        if (host, port) in self._closed_connections:
            connection = self._closed_connections[(host, port)]
            connection.sock = sock
            connection.length = length
            connection.brightness = self._brightness
            connection._closed = False
            del self._closed_connections[(host, port)]
        else:
            connection = LightConnection(sock, length,
                                         brightness=self._brightness)

        self.connections[(host, port)] = connection

        return connection

    async def _signal_watcher(self, *signals):
        with trio.open_signal_receiver(*signals) as sigset_aiter:
            async for signum in sigset_aiter:
                raise SignalCaughtError(signum)

    async def __aenter__(self):
        self._nursery_context_manager = trio.open_nursery()
        self._nursery = await self._nursery_context_manager.__aenter__()
        self._nursery.start_soon(self._signal_watcher, signal.SIGTERM)
        return self

    async def __aexit__(self, exc_type, exc, traceback):
        try:
            await self._nursery_context_manager.__aexit__(
                exc_type, exc, traceback)
        finally:
            await self.close_all()
            self._nursery_context_manager = None
            self._nursery = None

    def __enter__(self):
        raise RuntimeError(
            "use 'async with {name}(...)', not 'with {name}(...)'".format(
                name=self.__class__.__name__))

    def __exit__(self):
        assert False, """Never called, but should be defined"""


class SignalCaughtError(Exception):
    def __init__(self, signal):
        self.signal = signal

    def __str__(self):
        return "<{} signal={}>".format(
            self.__class__.__name__, signal.Signals(self.signal).name)

    def __repr__(self):
        return str(self)
