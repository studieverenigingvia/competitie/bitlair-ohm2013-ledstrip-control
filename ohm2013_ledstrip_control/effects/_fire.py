from .. import Effect
import random

__all__ = ["FireEffect"]


class FireEffect(Effect):
    rdata = []
    gdata = []
    bdata = []

    pcount = 300

    def __init__(self):
        super().__init__(step_time=0.02)

    def setup(self, strip):
        (h, w) = strip.shape
        self.particles = [Particle(self, strip.shape,
                                   random.randint(0, w - 1),
                                   random.randint(0, h))
                          for each in range(self.pcount)]

        for i in range(150):
            self.rdata.append(0)
            self.gdata.append(0)
            self.bdata.append(0)

    def step(self, strip, count):
        for i in range(self.pcount):
            self.particles[i].updateparticle()

        for i in range(150):
            x = (149 - i) % 7
            y = min(20, (149 - i) // 7)
            strip[y, x] = (self.rdata[i], self.gdata[i], self.bdata[i])

        self.cleanarray()

    def cleanarray(self):
        for i in range(150):
            self.rdata[i] = 0
            self.gdata[i] = 0
            self.bdata[i] = 0


class Particle:

    def __init__(self, fire, shape, x, y):
        self.rgb = (255, 255, random.randint(0, 255))
        self.fire = fire
        self.shape = shape
        self.height = shape[0]
        self.y = y
        self.x = x
        self.rnderp = id(self) % 9
        self.speed = (self.rnderp / 18) + 1
        self.life = random.uniform(1, self.height - 5)

    def updateparticle(self):
        # Fire goes from white -> yellow -> deep orange
        if self.rgb[2] > self.rnderp + 30:
            self.rgb = (255, 255, self.rgb[2] - int(self.rnderp + 30))
        else:
            if self.rgb[1] > self.rnderp + 32:
                self.rgb = (255, self.rgb[1] - int(self.rnderp + 30), 0)
            else:
                if self.rgb[0] > self.rnderp + 5:
                    self.rgb = (self.rgb[0] - int(self.rnderp + 1),
                                self.rgb[1], 0)

        self.y -= self.speed

        intx = int(self.x)
        inty = int(self.y)

        self.intoarray(intx, inty, self.rgb)
        if self.y < self.life or self.y < 0.5:
            self.__init__(self.fire, self.shape, self.x, self.height)

    def intoarray(self, x, y, rgb):
        self.fire.rdata[y * 7 + x] = rgb[0]
        self.fire.gdata[y * 7 + x] = rgb[1]
        self.fire.bdata[y * 7 + x] = rgb[2]
