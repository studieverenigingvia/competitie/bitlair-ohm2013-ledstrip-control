from .. import Effect


class RainbowEffect(Effect):

    def __init__(self):
        super().__init__(step_time=0.1)

    def setup(self, strip):
        pattern = [(255, 0, 0),
                   (255, 102, 0),
                   (255, 255, 0),
                   (0, 255, 0),
                   (0, 0, 255),
                   (128, 0, 255),
                   (255, 0, 255),
                   (72, 0, 130),
                   (134, 0, 255),
                   ]
        (h, w) = strip.shape
        num_colors = len(pattern)
        for x in range(w):
            for y in range(h):
                strip[y, x] = pattern[x % num_colors]

    def step(self, strip, count):
        (h, w) = strip.shape
        for y in range(h):
            c = strip[y, w - 1]
            for x in reversed(range(w - 1)):
                strip[y, x + 1] = strip[y, x]

            strip[y, 0] = c
