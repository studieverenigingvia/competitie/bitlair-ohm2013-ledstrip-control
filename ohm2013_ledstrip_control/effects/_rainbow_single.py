from .. import Effect

__all__ = ["RainbowSingleEffect"]


class RainbowSingleEffect(Effect):
    period = 1800
    period13 = period / 3
    period23 = 2 * period / 3

    period16 = period / 6
    period26 = 2 * period / 6
    period36 = 3 * period / 6
    period46 = 4 * period / 6
    period56 = 5 * period / 6

    def __init__(self):
        super().__init__(step_time=0.01)

    def _get_color_value(self, count):
        while count < 0:
            count += self.period
        while count >= self.period:
            count -= self.period

        if count < self.period16:
            return 255
        if count < self.period26:
            count -= self.period16
            return 255 * (self.period16 - count) / self.period16
        if count < self.period46:
            return 0
        if count < self.period56:
            count -= self.period46
            return 255 * count / self.period16
        if count < self.period:
            return 255
        return 0

    def _rainbow(self, count):
        r = self._get_color_value(count)
        g = self._get_color_value(count - self.period13)
        b = self._get_color_value(count - self.period23)
        return (r, g, b)

    def step(self, strip, count):
        strip.fill(self._rainbow(count % self.period))
