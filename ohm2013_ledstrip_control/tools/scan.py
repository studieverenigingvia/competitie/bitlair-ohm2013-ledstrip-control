from ohm2013_ledstrip_control import LightConnectionManager, LightEventType

import argparse
import re
import trio

HOST_PORT_REGEX = re.compile(r"(?P<host>.+):(?P<port>\d+)")


def get_host_port(addr):
    m = HOST_PORT_REGEX.match(addr)
    if m:
        host = m.group('host')
        port = int(m.group('port'))
    else:
        host = addr
        port = 6454

    return (host, port)


def timeout(arg):
    arg = float(arg)
    if arg <= 0:
        raise argparse.ArgumentTypeError("Timeout should be greater than zero")
    return arg


async def run_async(args):
    bind_host, bind_port = get_host_port(args.bind_address)

    addrs = [get_host_port(addr) for addr in args.addresses]

    lights = {}

    async def poll_reply(m, host, port, mac, is_reconnect):
        if mac not in lights:
            lights[mac] = (host, port)

    if not args.csv:
        print("Sending poll requests to:")
        for host, port in addrs:
            print("    {}:{}".format(host, port))

    with trio.move_on_after(args.timeout + 1):
        async with LightConnectionManager() as m:
            m.register_event_handler(LightEventType.NEW_POLL_REPLY, poll_reply)

            m.run_poll(addrs, bind_host=bind_host, bind_port=bind_port,
                       reply_timeout=args.timeout)
            if not args.csv:
                print("Waiting for replies...")

    if args.csv:
        print("host,port,mac")
        for mac, (host, port) in lights.items():
            print("{},{},{}".format(host, port, mac))
    else:
        reply_macs = set()
        print("\nReplies:")
        for mac, (host, port) in lights.items():
            print("    {}:{} - {}".format(host, port, mac))
            reply_macs.add(mac.upper())
        print("\nNumber of replies: {}".format(len(lights)))

        if args.report_no_reply:
            should_reply = set(m.upper().strip()
                               for m in args.report_no_reply)
            no_reply = should_reply - reply_macs
            if len(no_reply) > 0:
                print("\nLights that did not reply:")
                for mac in sorted(no_reply):
                    print("    {}".format(mac))


def run():
    parser = argparse.ArgumentParser(
        description="Scan network for OHM2013 Ledstrips.")

    parser.add_argument('addresses', type=str, nargs='+', metavar='ADDRESS',
                        help=('One or more addresses to scan on. '
                              'Can be both in the format \'<host>\''
                              ' or \'<host>:<port>\'. '
                              'The port defaults to 6454.'))
    parser.add_argument('--bind-address', '-b', type=str, help=(
        'Address to bind on. Can be both in the format \'<host>\''
        ' or \'<host>:<port>\'. Defaults to 0.0.0.0:6454.'
    ), default='0.0.0.0:6454')
    parser.add_argument('--timeout', '-t', help=(
        "Amount of seconds to wait for a poll reply. Defaults to 10"
    ), default=10, type=timeout)
    parser.add_argument('--csv', action='store_true', help=(
        "Output the results in a CSV format, without any extra information"))
    parser.add_argument("-r", "--report-no-reply", type=argparse.FileType('r'))

    args = parser.parse_args()

    try:
        trio.run(run_async, args)
    except KeyboardInterrupt:
        if not args.csv:
            print("Abort")
        exit(1)
