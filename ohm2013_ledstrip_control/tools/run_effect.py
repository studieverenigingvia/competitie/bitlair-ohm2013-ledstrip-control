import trio
import argparse

from ohm2013_ledstrip_control import LightConnectionManager
from ohm2013_ledstrip_control.effects import FireEffect, \
    RainbowEffect, RainbowSingleEffect


effects = {
    "fire": FireEffect,
    "rainbow": RainbowEffect,
    "rainbow-single": RainbowSingleEffect
}


async def run_event_loop(args):
    effect = effects[args.effect]

    async with LightConnectionManager() as m:
        m.brightness = args.brightness

        lights = []
        for h in args.hosts:
            light = await m.connect(h, args.port)
            lights.append(light)

        m.run_effect(effect(), lights)


def run():
    parser = argparse.ArgumentParser(
        description="Run an effect on one or OHM2013 Ledstrips.")

    parser.add_argument('hosts', metavar='HOST', type=str, nargs='+',
                        help='Host address(es) to send to.')
    parser.add_argument('--port', '-p', metavar='PORT', type=int,
                        default=6454, help='Port that the light(s) run on.')
    parser.add_argument('--brightness', '-b', metavar='BRIGHTNESS', type=float,
                        default=1, help='Lights brightness factor.'
                        ' Should be between 0 and 1.')
    parser.add_argument("--effect", '-e', metavar="EFFECT",
                        choices=effects.keys(), default="fire",
                        help="Effect to run. choices: {}. Default: fire"
                        .format(", ".join(effects.keys())))
    args = parser.parse_args()

    if args.brightness < 0 or args.brightness > 1:
        print("Error: brightness should be between 0 and 1")
        exit(1)

    try:
        trio.run(run_event_loop, args)
    except KeyboardInterrupt:
        pass
