import argparse
import math
import sys
import os
import select
import socket

import signal
from multiprocessing import Process

import pygame


have_xlib = False
verbose = False

if sys.platform == 'linux':
    try:
        from Xlib import display
        have_xlib = True
    except ImportError:
        print("Warning: Xlib not available")
        pass


def calculate_scale():
    scale = 1
    if have_xlib:
        d = display.Display()
        screen = d.screen()
        w = screen.width_in_pixels
        h = screen.height_in_pixels
        w_mm = screen.width_in_mms
        h_mm = screen.height_in_mms
        diagonal = math.sqrt(w ** 2 + h ** 2)
        diagonal_mm = math.sqrt(w_mm ** 2 + h_mm ** 2)
        diagonal_inch = diagonal_mm * 0.03937007874

        ppi = diagonal / diagonal_inch
        scale = ppi / 96

    return scale


class SimStrip:
    port = 0
    sock = 0
    screen = object()

    def __init__(self, num, host, port, window_scale):
        self.host = host
        self.port = port
        self.num = num
        sys.stdout.flush()

        addrs = [addr[0]
                 for (family, _, _, _, addr)
                 in socket.getaddrinfo(host, port)
                 if family == socket.AF_INET]
        if len(addrs) == 0:
            self.log("Error: host '{}' does not resolve to an IPv4 address".
                     format(host))
            exit(1)

        addr = addrs[0]
        mac_addr = 0x1A2B00000000
        addr_bin = 0
        for octed in addr.split('.'):
            addr_bin = addr_bin << 8 | int(octed)

        mac_addr |= addr_bin
        self.mac_addr_bytes = [mac_addr >> 8 * i & 0xff
                               for i in range(5, -1, -1)]
        mac_addr_str = ":".join("{:02X}".format(b)
                                for b in self.mac_addr_bytes)

        self.log("Running on {}:{} with MAC address {}".format(
            host, port, mac_addr_str))

        # Create UDP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.sock.bind((self.host, self.port))

        self.screen = Screen("L{} @ {}:{}".format(
            num, '' if self.host == '0.0.0.0' else host, self.port),
            window_scale)

        self.poll_reply_msg = self.create_poll_reply_msg()

    def log(self, msg):
        print("L{}: {}".format(self.num, msg))

    def log_verbose(self, msg):
        if verbose:
            self.log(msg)

    def close(self):
        self.sock.close()

    def create_poll_reply_msg(self):
        msg = bytearray(b"Art-Net\x00\x00\x21")
        ip, _ = self.sock.getsockname()
        ip_bytes = [int(b) for b in ip.split(".")]
        msg.extend(ip_bytes)
        ip_bin = ip_bytes[0] << 24 | ip_bytes[1] << 16 | \
            ip_bytes[2] | ip_bytes[3]

        msg.extend(b"\x36\x19\x05\x57")

        msg.append((ip_bin & 0x7f00) >> 8)
        msg.append((ip_bin & 0xf0) >> 4)

        msg.extend(b"\x00\x00\x00\x00LO")

        # Short name
        msg.extend(b"LED strip sim" + b"\x00" * 5)

        # Long name
        msg.extend(b"LED strip controller for OHM 2013 simulator" +
                   b"\x00" * 21)

        # Number of ports
        msg.extend(b"\x00" * 64)
        msg.extend(b"\x00\x02")

        # Port types
        msg.extend(b"\x80\x80\x00\x00")

        # GoodInput
        msg.extend(b"\x00\x00\x00\x00")
        # GoodOutput
        msg.extend(b"\x00\x00\x00\x00")
        # SwIn
        msg.extend(b"\x00\x00\x00\x00")

        # SwOut
        msg.extend(b"\x00")
        msg.append(ip_bin & 15)
        msg.extend(b"\x00\x00")

        # SwVideo, SwMacro, SwRemote, Spare
        msg.extend(b"\x00\x00\x00\x00\x00\x00")

        # Style
        msg.extend(b"\x00")

        # MAC address (1A:2A:xx:xx:xx:xx)
        msg.extend(self.mac_addr_bytes)

        # Bind IP
        msg.extend(ip_bytes)

        # BindIndex, Status2, Filler
        msg.extend(b"\x00\x60" + b"\x00" * 26)

        return msg

    def handle_messages(self):
        self.sock.setblocking(0)
        while True:
            self.screen.process_events()
            ready = select.select([self.sock], [], [], 0.01)
            if ready[0]:
                rdata, addr = self.sock.recvfrom(5000)
                if rdata[8] == 0x00 and rdata[9] == 0x20:
                    self.log_verbose(
                        "Received poll request from {}:{}".format(*addr))

                    self.sock.sendto(self.poll_reply_msg, 0, addr)
                if rdata[8] == 0x00 and rdata[9] == 0x21:
                    # Ignore for now
                    pass
                if rdata[8] == 0x00 and rdata[9] == 0x50:

                    # draw pixels
                    for i in range(0, 7 * 21):
                        x = 6 - (i % 7)
                        y = i // 7
                        r = rdata[18 + 3 * i + 9]
                        g = rdata[18 + 3 * i + 1 + 9]
                        b = rdata[18 + 3 * i + 2 + 9]
                        self.screen.draw(x, y, (r, g, b))

                    self.screen.update_screen()

        self.sock.setblocking(1)

    def hexdump(self, data):
        print(":".join("{:02x}".format(c) for c in data))


class Screen:
    def __init__(self, title, window_scale):

        self.pixelswide = 7
        self.pixelshigh = 21

        self.screenwide = int(window_scale * 20 * self.pixelswide)
        self.screenhigh = int(window_scale * 20 * self.pixelshigh)

        self.screen = pygame.display.set_mode(
            (self.screenwide, self.screenhigh))
        pygame.display.set_caption(title)
        self.surface = pygame.Surface((self.pixelswide, self.pixelshigh))

    def process_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

    def update_screen(self):
        pygame.transform.scale(self.surface,
                               (self.screenwide, self.screenhigh), self.screen)
        pygame.display.update()
        self.surface.fill((0, 0, 0))

    def draw(self, x, y, rgb):
        self.surface.set_at((x, y), rgb)


def exit_handler_child(signum, frame):
    sys.exit(0)


def run_single(num, host, port, window_scale):
    signal.signal(signal.SIGINT, exit_handler_child)
    signal.signal(signal.SIGTERM, exit_handler_child)
    signal.signal(signal.SIGQUIT, exit_handler_child)
    signal.signal(signal.SIGHUP, exit_handler_child)

    num_lights_per_row = 9
    x_off = 50
    y_off = 50
    pos_x = window_scale * (x_off + 200 * (num % num_lights_per_row))
    pos_y = window_scale * (y_off + 450 * (num // num_lights_per_row))
    os.environ['SDL_VIDEO_CENTERED'] = '0'
    os.environ['SDL_VIDEO_WINDOW_POS'] = '%i,%i' % (pos_x, pos_y)

    # Sometimes SDL takes over the audio device preventing other
    # audio applications to work. Telling SDL to use Pulse instead of ALSA
    # should (hopefully) fix this.
    os.environ['SDL_AUDIODRIVER'] = 'pulse'
    pygame.init()
    ss = SimStrip(num, host, port, window_scale)
    ss.handle_messages()


def exit_handler(signum, frame):
    if verbose:
        print("Exiting...")
    global processes
    for p in processes:
        p.terminate()

    sys.exit(0)


def run():
    parser = argparse.ArgumentParser(description='Simulate OHM2013 ledstrips')
    parser.add_argument('--port', '-p', metavar='PORT', type=int,
                        default=6454, help='Port to run the light(s) on.')
    parser.add_argument('--host', '-H', metavar='HOST', type=str, nargs='+',
                        help='Host address(es) to bind on.')
    parser.add_argument('--num-lights', '-l', metavar='N', type=int, default=1,
                        help='Amount of lights to be created.')
    parser.add_argument('--verbose', '-v', action='store_true',
                        help='Enable verbose output.')

    args = parser.parse_args()

    global verbose
    verbose = args.verbose

    window_scale = calculate_scale()

    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)
    signal.signal(signal.SIGQUIT, exit_handler)
    signal.signal(signal.SIGHUP, exit_handler)

    if args.host:
        num_hosts = len(args.host)
        if num_hosts != args.num_lights:
            print("Invalid value for host argument: {0} lights "
                  "require {0} hosts, {1} given.".format(
                      args.num_lights, num_hosts))
            sys.exit(1)
        else:
            hosts = args.host
    else:
        hosts = ["127.0.0.{}".format(i + 1) for i in range(args.num_lights)]

    global processes

    processes = []
    for i, host in enumerate(hosts):
        p = Process(target=run_single, args=(i, host, args.port,
                                             window_scale))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()
